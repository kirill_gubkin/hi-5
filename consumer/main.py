from kafka import KafkaConsumer

consumer = KafkaConsumer('my_topic', bootstrap_servers='localhost:9092')

message_count = 0

for _ in consumer:
    message_count += 1
    print("Прочитано сообщений:", message_count)

consumer.close()