from kafka import KafkaProducer
import datetime

producer = KafkaProducer(bootstrap_servers='localhost:9092')

for _ in range(1000):
    now = datetime.datetime.now()
    message = str(now)
    producer.send('my_topic', value=message.encode('utf-8'))

producer.close()